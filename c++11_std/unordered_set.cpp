#include <iostream>
#include <string>
#include <unordered_set>
#include <cassert>

using namespace std;

class Book {
public:
    Book(string title, string btype): title_(title), btype_(btype) {};

    // getter
    const std::string& get_title() const {
        return title_;
    }

    // operator
    bool operator ==(const Book & book) const {
        return title_ == book.title_ && btype_ == book.btype_;
    };

private:
    string title_;
    string btype_;
};

class BookHasher {
public:
    size_t operator()(const Book& book) const {
        return std::hash<std::string>()(book.get_title());
    }
};

int main() {

    // can pass a lambda?
    unordered_set<Book, BookHasher> books;

    // append book to books
    {
        Book b("foo", "comic");
        books.insert(b);
        books.insert(b);
        // unordered_set can only contain 1 item with the same hash
        assert(books.size() == 1);
        auto it = books.insert(Book("bar", "novel"));
        // TODO: use of it
    }

    // use of find
    auto it = books.find(Book("foo", "comic"));
    // TODO: compare

    // from:
    // https://stackoverflow.com/questions/35187543/pointer-reference-to-element-in-stdunordered-set
    // pointer to element not changed even after insertion

    int idx = 0;
    Book const* b_ptr_1 = nullptr;
    Book const* b_ptr_2 = nullptr;
    for(auto&& b: books) {
        if(idx == 0) {
            b_ptr_1 = &b;
        } else {
            b_ptr_2 = &b;
        }
        idx++;
    }

    assert(b_ptr_1 != nullptr);
    assert(b_ptr_1->get_title() == "foo" || b_ptr_1->get_title() == "bar");
    assert(b_ptr_2 != nullptr);
    assert(b_ptr_2->get_title() == "foo" || b_ptr_2->get_title() == "bar");
    assert(b_ptr_1->get_title() != b_ptr_2->get_title());
    std::cout << b_ptr_1->get_title() << " - " << b_ptr_2->get_title() << std::endl;

    books.insert(Book("baz", "dict"));

    assert(b_ptr_1 != nullptr);
    assert(b_ptr_1->get_title() == "foo" || b_ptr_1->get_title() == "bar");
    assert(b_ptr_2 != nullptr);
    assert(b_ptr_2->get_title() == "foo" || b_ptr_2->get_title() == "bar");
    assert(b_ptr_1->get_title() != b_ptr_2->get_title());
    std::cout << b_ptr_1->get_title() << " - " << b_ptr_2->get_title() << std::endl;

    return 0;
}

