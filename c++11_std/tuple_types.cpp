#include <tuple>
#include <string>
#include <cassert>

using namespace std;

int main() {

    tuple<int, string, bool> t { 1, "hello", false };
    assert(std::get<0>(t) == 1);

    // from Wikipedia
    // create tuple with type deduction
    auto record = std::make_tuple("Hari Ram", "New Delhi", 3.5, 'A');
    string name ; float gpa ; char grade ;

    // Use std::tie to unpack tuple +
    // std::ignore helps drop the place name
    tie(name, std::ignore, gpa, grade) = record ;
    assert(name == "Hari Ram");
    assert(gpa == 3.5);
    assert(grade == 'A');

    size_t tuple_item_count = tuple_size< std::tuple<int, string, bool, bool, int> >::value;
    assert(tuple_item_count == 5);
    // TODO: std::tuple_element<I, T>::type

    return 0;
}

