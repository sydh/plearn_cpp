#include <tuple>
#include <string>
#include <cassert>

using namespace std;

int main() {

    tuple<string, string, int> t("foo", "bar", 7);
    int i = std::get<int>(t);        // i == 7
    int j = std::get<2>(t);          // Same as before: j == 7
    assert(i == 7);
    assert(j == 7);

    // Uncomment this line to see compile error
    // string s = std::get<string>(t);  // Compile-time error due to ambiguity

    return 0;
}
