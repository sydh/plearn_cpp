#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

int main() {
    // Boolean type
    bool b0 = true; // == 1
    bool b1 = false; // == 0
    printf("b0: %d\n", b0);

    // Boolean operator on bool type
    assert( b0 == true );
    assert( b0 == 1 );
    assert( b1 == 0 );
    assert( (b0 && b1) == false );
    assert( (b0 || b1) == true );
    assert( (!b0) == false );



    return 0;
}