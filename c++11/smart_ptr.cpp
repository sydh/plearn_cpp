/*
 * Based on:
 * https://www.internalpointers.com/post/beginner-s-look-smart-pointers-modern-c
 */

#include <iostream>
#include <string>
#include <memory>
#include <cassert>

class Book {
public:
    Book(std::string title, std::string btype): title_(title), btype_(btype) {
        std::cout << "Constructor Book " << title << std::endl;
    };

    ~Book() {
        std::cout << "Destructor Book " << title_ << std::endl;
    };

    // getter
    const std::string& get_title() const {
        return title_;
    }

private:
    std::string title_;
    std::string btype_;
};

class Player {
public:
    ~Player() {
        std::cout << "Destructor Player" << std::endl;
    }
    std::shared_ptr<Player> buddy;
};

class Player2 {
public:
    ~Player2() {
        std::cout << "Destructor Player 2" << std::endl;
    }
    std::weak_ptr<Player2> buddy;
};

int main() {

    // Intro
    // Smart pointer provides automatic memory management
    // When a smart pointer goes out of scope, its destructor gets triggered
    // 3 kinds:
    // std::unique_ptr
    // std::shared_ptr
    // std::weak_ptr

    // 1
    // unique_ptr

    std::unique_ptr<int> up1(new int); // unique_ptr on integer
    *up1 = 40;
    assert(*up1 == 40);
    std::unique_ptr<int[]> up2(new int[32]); // unique_ptr on integer array of size 32
    std::unique_ptr<Book> up3(new Book("Watchmen vol 1", "Comic")); // unique_ptr on allocated Book object
    assert(up3->get_title() == "Watchmen vol 1");
    int *p1 = up1.get(); // get original pointer
    assert(p1 != nullptr);
    assert(*p1 == 40);

    // Note: better to use make_unique
    // WARNING: only avail in C++ 14
#if 0
    std::unique_ptr<int> up4 = std::make_unique<int>(); // unique_ptr on integer
    std::unique_ptr<int[]> up5 = std::make_unique<int[]>(16); // unique_ptr on integer array
    std::unique_ptr<Book> up6 = std::make_unique<Book>("Watchmen vol 2", "Comic"); // unique_ptr on Book
#endif

    // Note: could not copy an unique pointer
    // std::unique_ptr<int> up1_1 = up1;
    std::unique_ptr<int> up1_2 = std::move(up1); // move is ok
    assert(up1 == nullptr); // check now that up1 is not valid anymore
    assert(*up1_2 == 40);

    // 2
    // shared_ptr
    std::shared_ptr<int> shp1(new int);
    std::shared_ptr<Book> shp3(new Book("Watchmen vol 3", "Comic"));

    std::shared_ptr<int> shp4 = std::make_shared<int>();
    std::shared_ptr<Book> shp6 = std::make_shared<Book>("Watchmen vol 4", "Comic");

    // Note: prior to C++ 17, shared_pointer call delete and not delete[]
    // This can be fixed by specifying a lambda to properly delete the array
    // Note2: Cannot be specified using std::make_shared
    auto l = [](const int* i) { delete[] i; };
    std::shared_ptr<int[]> shp2(new int[8], l);

    // use_count(): number of references
    *shp1 = 7;
    assert(shp1.use_count() == 1);
    std::shared_ptr<int> shp1_1 = shp1;
    assert(shp1.use_count() == 2);
    assert(shp1_1.use_count() == 2);
    assert(*shp1_1 == 7);

    // Note: mind the circular reference
    std::shared_ptr<Player> bob = std::make_shared<Player>();
    std::shared_ptr<Player> alice = std::make_shared<Player>();
    bob->buddy = alice;
    // Comment this line to turn off Valgrind errors
    alice->buddy = bob; // Circular ref - no destructor will be called - memory leak

    // 3
    // weak_ptr
    // Note: a weak ptr can only be created from a shared_ptr
    std::shared_ptr<int> shp7 = std::make_shared<int>(42);
    assert(*shp7 == 42);
    assert(shp7.use_count() == 1);
    std::weak_ptr<int> wp1 = shp7;
    assert(shp7.use_count() == 1); //  weak pointer does not increment reference count

    std::shared_ptr<int> shp7_orig = wp1.lock(); // get back the original shared pointer

    shp7.~shared_ptr<int>();
    shp7_orig.~shared_ptr(); // now ref count should be 0 ;-)
    assert(wp1.expired()); // Check that the original data has been deleted

    // Note: using weak ptr in Player2 class avoid creating circular ref
    std::shared_ptr<Player2> bob2 = std::make_shared<Player2>();
    std::shared_ptr<Player2> alice2 = std::make_shared<Player2>();
    bob2->buddy = alice2;
    alice2->buddy = bob2;

    return 0;
}
