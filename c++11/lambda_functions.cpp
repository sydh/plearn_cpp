#include <iostream>
#include <vector>
#include <cassert>
#include <algorithm>
#include <map>
#include <functional>

int main() {

    std::vector<int> v{1, 2, 3, 4};
    // WARNING: transform requires that the target container has been resized
    // here we declare the vector with the required size
    std::vector<int> v_plus_1(4);
    // declare a lambda func
    auto plus_1 = [](int &i) { return i+1; };

    std::transform(
            v.begin(), v.end(),
            v_plus_1.begin(),
            plus_1
    );

    assert(v[0] == 1); assert(v[3] == 4);
    assert(v_plus_1[0] == 2); assert(v_plus_1[3] == 5);

    int n = 4;
    int m = 1;
    std::vector<int> v_plus_n;
    v_plus_n.resize(v.size());

    // lambda captures variable 'n'
    auto plus_n_0 = [n](int &i) { return i+n; }; // n captured by value
    auto plus_n_1 = [&n](int &i) { return i+n; }; // n captures by reference

    // build a vector of lambdas
    std::vector< std::function<int(int&)> > lv { plus_n_0, plus_n_1 };

    for(auto &l: lv) {
        std::vector<int> v2;
        v2.resize(v.size());

        std::transform(
                v.begin(), v.end(),
                v2.begin(),
                l
        );

        assert(v[0] == 1); assert(v[3] == 4);
        assert(v2[0] == 5); assert(v2[3] == 8);
    }

    auto plus_n_minus_m_0 = [&](int &i) { return i+n-m; }; // all var captured by ref
    auto plus_n_minus_m_1 = [&, m](int &i) { return i+n-m; }; // all var captured by ref but m is captured by value
    auto plus_n_minus_m_2 = [=, &n](int &i) { return i+n-m; }; // all var captured by value but n is captured by ref

    std::vector< std::function<int(int&)> > lv2 { plus_n_minus_m_0, plus_n_minus_m_1, plus_n_minus_m_2 };

    std::map<int, std::function<int(int&)> >lm1 {
        {0, plus_n_minus_m_0},
        {1, plus_n_minus_m_1},
        {2, plus_n_minus_m_2}
    };

    assert( lm1.find(0)->second(v[0]) == 4 );

    // TODO: convert capturless lambda to function ptr

}
