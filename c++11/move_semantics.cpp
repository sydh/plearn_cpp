/*
 * Based on blog:
 * https://www.internalpointers.com/post/c-rvalue-references-and-move-semantics-beginners
 *
 * + some improvements:
 *
 * noexcept for move constructor & move assignment:
 * https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#f6-if-your-function-may-not-throw-declare-it-noexcept
 *
 */

#include <iostream>
#include <cassert>

class Holder {
public:
    Holder(int size) {
        std::cout << "Constructor..." << std::endl;
        data_ = new int[size];
        size_ = size;
        std::cout << "done." << std::endl;
    }
    ~Holder() {
        std::cout << "Destructor..." << std::endl;
        delete[] data_;
        std::cout << "done." << std::endl;
    }

    // getter
    int size() {
        return size_;
    }

    // copy constructor -> Holder h2(h1);
    Holder(const Holder& other) {
        std::cout << "Copy constructor..." << std::endl;
        data_ = new int[other.size_];
        std::copy(other.data_, other.data_+other.size_, data_);
        size_ = other.size_;
        std::cout << "done." << std::endl;
    }

    // assignment operator -> Holder h2 = h1;
    Holder& operator=(const Holder &other) {
        std::cout << "Assignment operator..." << std::endl;
        if(this == &other) {
            // check for auto assignment, do nothing...
            return *this;
        }
        delete[] data_;
        data_ = new int[other.size_];
        std::copy(other.data_, other.data_+other.size_, data_);
        size_ = other.size_;
        std::cout << "done." << std::endl;
        return *this;
    }

    // move constructor
    Holder(Holder&& other) noexcept {
        std::cout << "Move constructor..." << std::endl;
        data_ = other.data_;
        size_ = other.size_;
        other.data_ = nullptr;
        other.size_ = 0;
        std::cout << "done." << std::endl;
    }

    // move assignment operator
    Holder& operator=(Holder&& other) noexcept {
        std::cout << "Move assignment operator..." << std::endl;
        if(this == &other) {
            return *this;
        }
        data_ = other.data_;
        size_ = other.size_;
        other.data_ = nullptr;
        other.size_ = 0;
        std::cout << "done." << std::endl;
        return *this;
    }

private:
    int* data_;
    int size_;
};

Holder createHolder(int size)
{
    // Note: without move semantics, a copy is done
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    return Holder(size);
}


int main() {
    Holder h1(1024); // regular constructor
    Holder h2(h1); // copy constructor
    assert(h2.size() == h1.size());
    Holder h3(256);
    h3 = h1; // assignment operator
    assert(h3.size() == h1.size());

    std::cout << "===" << std::endl;
    Holder h4 = createHolder(512); // move constructor
    std::cout << "===" << std::endl;
    h3 = createHolder(128); // move assignment operator
    std::cout << "===" << std::endl;
    Holder h5(std::move(h4)); // force using move constructor
    assert(h4.size() == 0);
    std::cout << "===" << std::endl;

    return 0;
}
