#include <iostream>
#define _USE_MATH_DEFINES
#include <cmath>
#include <limits>
#include <array>
#include <cassert>

// Old style constant

#define TAU_1 (2*M_PI);

// With constant

const double TAU_2 = 2*M_PI; // Ok, value computed @ run time

// With constant expression
constexpr double TAU_3 = 2*M_PI; // Ok, value computed @ compile time

// Constexpr function
constexpr double degree_to_radians(double angle) {
    return angle * M_PI / 180.0;
}

// And use it as constexpr
constexpr double a180 = degree_to_radians(180.0);
constexpr double a360 = degree_to_radians(360.0);

class Foo {
public:
    int msize() { return msize_; };
    int value() { return value_; };
private:

    // Prior to C++ 11 only integral or enum type can be static
    static const int msize_ = 99;
    // for types: float or double, it requires constexpr
    constexpr static const double value_ = 9.0;
};

int main() {

    double tau = TAU_1
    std::cout << "TAU 1: " << tau << std::endl;
    std::cout << "TAU 2: " << TAU_2 << std::endl;
    std::cout << "TAU 3: " << TAU_3 << std::endl;

    // Diff between const and constexpr
    const short mx1 = std::numeric_limits<short>::max(); // Ok but runtime initialisation
    // const short mx1 = 10;
    constexpr short mx2 = std::numeric_limits<short>::max(); // Ok but runtime initialisation
    int arr1[mx1];
    int arr2[mx2];

    assert(arr1 != nullptr);
    static_assert(arr2 != nullptr, "");

    std::array<int, mx1> arr1_1;
    assert(arr1_1.size() == mx1);

    std::cout << "Angle 180 in radians " << a180 << std::endl;
    std::cout << "Angle 360 in radians " << a360 << std::endl;

    Foo f;
    assert(f.msize() == 99);
    assert(f.value() == 9.0);

    return 0;
}