// Pass/Return smart pointers to function + conversion
// See smart_ptr.cpp first
// From:
// https://www.internalpointers.com/post/move-smart-pointers-and-out-functions-modern-c

#include <memory>
#include <cassert>

// Pass by value functions

void f1(std::unique_ptr<int> a) {
    assert(*a == 42);
}

void f2(std::shared_ptr<int> a) {
    // Check that there are 2 smart pointers
    assert(a.use_count() == 2);
}

void f3(std::weak_ptr<int> a) {
    if(std::shared_ptr<int> b = a.lock())
    {
        assert(b.use_count() == 2);
        *b += 1;
    }
}

// Pass by reference functions

void f4(std::unique_ptr<int>& a) {

    std::unique_ptr<int> a2(new int);
    *a2 = 7;
    a = std::move(a2);
}

// Pass raw pointer

void f5(int* a) {
    *a += 1;
}

// Pass reference

void f6(int& a) {
    a += 1;
}

// Return by value

std::unique_ptr<int> f7(int v) {
    return std::unique_ptr<int>(new int(v));
}

int main() {

    // Pass by value: unique_ptr
    std::unique_ptr<int> up1(new int(41));
    *up1 += 1;
    // Note: unique pointer cannot be copied, but move is ok
    // f1(up1);
    f1(std::move(up1));
    // Note: uncomment this line to crash the program
    // unique_ptr has been move and cannot be accessed or modified
    // *up1 += 1;

    // Pass by value: shared_ptr
    std::shared_ptr<int> shp1 = std::make_shared<int>(41);
    *shp1 += 1;
    assert(shp1.use_count() == 1);
    f2(shp1);
    // Note: here shp1 is still avail
    *shp1 += 1;
    assert(shp1.use_count() == 1);
    assert(*shp1 == 43);

    // Pass by value: weak_ptr
    f3(shp1);
    assert(shp1.use_count() == 1);
    assert(*shp1 == 44);

    // Pass by reference: unique_ptr
    // Pass by const ref is useless
    std::unique_ptr<int> up2(new int(42));
    f4(up2);
    assert(*up2 == 7);

    // Pass by reference: shared_ptr
    // same as unique_ptr
    // const ref is ok if function only reads from ptr

    // Pass by reference: weak_ptr
    // Useless

    // Pass raw pointers (can be null) or Pass reference (cannot be null)
    // To modify the underlying object without messing with smart pointer
    // apply to unique, shared and weak ptr
    f5(up2.get()); // pass raw pointers
    assert(*up2 == 8);
    f6(*shp1.get()); // pass reference
    assert(*shp1 == 45);

    // Return smart pointers
    // Should always return by value
    // Use move internally (for alloc object)
    // Use RVO for smart pointer itself (standard in C++ 17)
    // Return by ref a shared_ptr does not incr ref count

    std::unique_ptr<int> v = f7(42);
    *v += 1;
    assert(*v == 43);

    // Conversion
    // Conversion from unique_ptr -> shared_ptr -> OK
    // Warning: beware of custom deleter for unique_ptr
    std::unique_ptr<short> ups1(new short(14));
    // std::shared_ptr<short> shps1 = std::move(ups1);
    std::shared_ptr<short> shps1 {std::move(ups1)};
    assert(*shps1 == 14);

    // Conversion from shared_ptr -> unique_ptr -> not an option

    return 0;
}
