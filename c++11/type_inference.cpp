#include <iostream>
#include <vector>
#include <functional>
#include <typeinfo>
#include <cassert>

#include <type_traits>
#include <typeinfo>
#ifndef _MSC_VER
#   include <cxxabi.h>
#endif
#include <memory>
#include <string>
#include <cstdlib>

// From Stack Overflow
// https://stackoverflow.com/questions/81870/is-it-possible-to-print-a-variables-type-in-standard-c
// Note: C++ 14 && C++ 17 constexpr solution
template <class T>
std::string
type_name() {
    typedef typename std::remove_reference<T>::type TR;
    std::unique_ptr<char, void(*)(void*)> own
    (
        #ifndef _MSC_VER
        abi::__cxa_demangle(typeid(TR).name(), nullptr,
                            nullptr, nullptr),
     #else
     nullptr,
     #endif
     std::free
    );
    std::string r = own != nullptr ? own.get() : typeid(TR).name();
    if (std::is_const<TR>::value)
        r += " const";
    if (std::is_volatile<TR>::value)
        r += " volatile";
    if (std::is_lvalue_reference<T>::value)
        r += "&";
    else if (std::is_rvalue_reference<T>::value)
        r += "&&";
    return r;
}

void show_info(int a, int b, std::vector<int>& v) {
    // Just a dummy function
    std::cout << "a: " << a << " - b " << b << " - vec size: " << v.size() << std::endl;
}

int main() {
    
    // type_inference: keyword auto
    std::vector<int> v0(4);
    auto some_strange_callable_type = std::bind(&show_info, 2, 1, v0);
    some_strange_callable_type();
    auto other_variable = 5;
    
    // Warning: typeid discard qualifier like const, &, &&
    std::cout << "type 'some_strange_callable_type': " << typeid(some_strange_callable_type).name() << std::endl;
    std::cout << "type 'other_variable': " << typeid(other_variable).name() << std::endl;
    int& r1 = other_variable;
    // typeid discard & qualifier, see below for type_name function
    assert(typeid(other_variable).name() == typeid(r1).name());
    std::cout << "type 'r1': " << typeid(other_variable).name() << std::endl;
    
    // type inference: keyword decltype
    int some_int;
    decltype(some_int) other_integer_variable = 5;

    const std::vector<int> v(1);
    auto a = v[0];        // a has type int
    decltype(v[0]) b = 1; // b has type const int&, the return type of
                          //   std::vector<int>::operator[](size_type) const
    auto c = 0;           // c has type int
    auto d = c;           // d has type int
    decltype(c) e;        // e has type int, the type of the entity named by c
    decltype((c)) f = c;  // f has type int&, because (c) is an lvalue
    decltype(0) g;        // g has type int, because 0 is an rvalue
    
    // TODO: decltype on function
    
    // using custom function type_name to output real type
    assert(type_name<decltype(other_variable)>() != type_name<decltype(r1)>());
    std::cout << "type name 'other_variable': " << type_name<decltype(other_variable)>() << std::endl;
    std::cout << "type name 'r1': " << type_name<decltype(r1)>() << std::endl;
 
    // some assert
    assert(type_name<decltype(f)>() == "int&");
}
