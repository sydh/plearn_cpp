#include <iostream>
#include <cassert>
#include <vector>

void fn1(const int& r) {
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    std::cout << "r: " << r << std::endl;
}

class Person {
public:
    // Constructor
    Person(std::string name, int age): name_(name), age_(age) {
        std::cout << "Person Constructor " << name << std::endl;
    };

    ~Person() noexcept {
        std::cout << "Drop Person " << name_ << std::endl;
    }

    // Move constructor
    Person(Person&& other) noexcept: name_(other.name_), age_(other.age_) {
        std::cout << "Person Move Constructor " << other.name_ << std::endl;
    };

    // Person& operator=(const Person& other) = default;
    void dump() const {
        std::cout << "Person `" << name_ << "`, age: " << age_ << std::endl;
    }

    /*
    bool is_named(const std::string& name) const {
        return (name == name_);
    }
     */

private:
    std::string name_;
    int age_;
};

int main() {

    // lvalue versus rvalue (C++ 03)
    // lvalue ~ `container`
    // rvalue ~ `things contained in container`

    int i = 0; // 'i' is a lvalue, 0 is a rvalue
    int* pi = &i; // pi is a lvalue, '&' operator return a rvalue from lvalue
    assert(*pi == 0);
    int& ri = i; // ri is a lvalue reference to i
    ri++; // modify i using the reference
    assert(i==1);
    assert(ri==1);

    // int& ri2 = 10 // ILLEGAL: otherwise we could modify constant
    const int& r10 = 10; // Ok: Const reference
    assert(r10 == 10);
    fn1(10); // Ok: argument is a const reference

    // rvalue reference (C++ 11)
    std::string s1("hello");
    std::string s2(" world");
    std::string s3 = s1 + s2; // s1 + s2 create a temporary object to hold the return of operator +
    assert(s3 == "hello world");
    std::string&& rv = s1 + s2; // reference to temporary object -> reference rvalue -> rvalue reference
    const std::string&& rv2 = s1 + s2; // const rvalue reference
    assert(rv == "hello world");
    assert(rv2 == "hello world");
    std::cout << "rv: " << rv << std::endl;
    std::cout << "rv2: " << rv2 << std::endl;
    rv += " !!"; // rv is a rvalue reference so we can modify it
    assert(rv == "hello world !!");
    std::string s4_1 (rv); // copy from rvalue reference
    assert(!rv.empty()); // check copy
    std::string s4_2 (std::move(rv)); // move rvalue reference to s4_2
    assert(s4_1 == "hello world !!");
    assert(rv.empty());

    // practical use of rvalue reference
    // for move semantics

    // example 1: emplace back
    // https://en.cppreference.com/w/cpp/container/vector/emplace_back

    std::cout << "=== EXAMPLE 1 ===" << std::endl;

    std::vector<Person> vips;
    std::cout << "emplace_back" << std::endl;
    vips.emplace_back("Gandalf", 330); // Only one Constructor, emplace_back use rvalue ref as arguments
    std::cout << "push back" << std::endl;
    vips.push_back(Person("Soron", 999)); // Constructor + Move + Move, clang-tidy warns about this
    // contents
    for(auto& vip: vips) {
        vip.dump();
    }

    // example 2: loop over rvalue reference
    // inspiration:
    // https://stackoverflow.com/questions/13130708/what-is-the-advantage-of-using-forwarding-references-in-range-based-for-loops
    // https://blog.petrzemek.net/2016/08/17/auto-type-deduction-in-range-based-for-loops/

    std::cout << "=== EXAMPLE 2 ===" << std::endl;
    std::vector<bool> vb(10);
    // Using auto& b does not compile
    // Error: cannot bind non-const lvalue reference of type ‘...’ to an rvalue of type ’...’
    // Eg. iterator for vector<bool> returns rvalue reference so that must be explicit in the for loop
    // NOTE:
    // Recommendation is to use for(auto&& x: ...) only in generic code

    for(auto&& b: vb) {
        b = true;
    }

}
