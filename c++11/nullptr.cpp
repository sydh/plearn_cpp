#include <iostream>
#include <cassert>

int foo(char *str) {
    std::cout << "foo char *" << std::endl;
    return 1;
}

int foo(int) {
    std::cout << "foo int" << std::endl;
    return 2;
}

int bar(int) {
    std::cout << "bar" << std::endl;
    return 3;
}

int main() {

    // NULL is often defined as 0 so it can cause issue with function overloading
    // here is an example
    int res1 = foo(nullptr);
    // This is not even allowed by g++: call of overloaded ... is ambiguous
    // int res2 = foo(NULL);
    // This one is ok but with a warning (at least for g++)
    int res3 = bar(NULL);

    assert(res1 == 1);
    // assert(res2 == 2);
    assert(res3 == 3);

    // Note: nullptr can be compared to bool
    bool b = nullptr;
    assert(b == false);
}
