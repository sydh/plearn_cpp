#include <iostream>
#include <cassert>
#include <string>
#include <sstream>

// old style enum

enum En0 {
    val1 = 1,
    val2,
    val3
};

// will not compile as val3 already declared in En0 enum
// not a problem with class based enum
/*
enum En0_2 {
    v0,
    v1,
    val3
}
*/

// c++ 11 class ennum
// Default type: int
// Warning: cannot add method to enum, operator are ok
enum class En1 {
    val1 = 0,
    val2,
    val3
};

En1& operator++(En1& e1)
{
    // Allow:
    // En1 e = En1::val2;
    // ++e; // e is now En1::val3
    switch(e1)
    {
        case En1::val1: return e1 = En1::val2;
        case En1::val2: return e1 = En1::val3;
        case En1::val3: return e1 = En1::val1;
        default:
            // Prevent compiler warning: control reaches end of non-void function
            return e1 = En1::val1;
    }
}

std::ostream& operator<<(std::ostream& os, const En1& e1)
{
    switch(e1) {
        case En1::val1:
            os << "En1::val1";
            break;
        case En1::val2:
            os << "En1::val2";
            break;
        case En1::val3:
            os << "En1::val3";
            break;
        default:
            // FIXME: not sure, what this is...
            os.setstate(std::ios_base::failbit);
    }
    return os;
}

// With explicit type
// Cannot declare enum with type like char*, char[]
enum class En2: unsigned int {
    val1,
    val2,
    val3 = 100,
    val4,
};

enum class En3: char {
    val1 = 'a',
    val2 = 'b',
};

// With initializer_lists
enum class En4: int {val1=1, val2};

int main() {

    std::cout << "c++ 11 - strongly typed enumerations" << std::endl;

    En0 e0 = En0::val3;
    assert(static_cast<int>(e0) == 3);
    En1 e1(En1::val1);
    std::cout << "e1: " << e1 << std::endl;
    std::cout << "++e1: " << ++e1 << std::endl;
    assert(++e1 == En1::val3);

    // init Enum from int using static_cast
    En1 e2 = static_cast<En1>(1);
    std::ostringstream stream;
    stream << e2;
    std::string e2_str =  stream.str();
    assert(e2_str == "En1::val2");

    // static_assert done @ compile time
    // Note: message is optional in C++ 17
    static_assert(static_cast<int>(En1::val1) == 0, "");
    static_assert(static_cast<int>(En2::val3) == 100, "");
    static_assert(static_cast<int>(En2::val4) == 101, "");
    static_assert(static_cast<char>(En3::val1) == 'a', "");
    static_assert(static_cast<char>(En3::val2) == 'b', "");
    static_assert(static_cast<char>(En4::val2) == 2, "");

    // FIXME: why is this valid? No warnings from compiler?
    En3 e3 = static_cast<En3>('c');
    std::cout << "e3: " << static_cast<char>(e3) << std::endl;

    return 0;
}
