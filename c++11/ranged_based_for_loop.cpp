#include <vector>
#include <cassert>
#include <iostream>

int main() {

    std::vector<int> v {1, 2, 3, 4};

    // Warning: copy element, better use auto&
    std::cout << "display v:" << std::endl;
    for(auto i: v) {
        std::cout << "item: " << i << std::endl;
    }

    // iter over vector as reference
    // same as: for(int& i: v) {...}
    for(auto& i: v) {
        i+= 1;
    }

    assert(v[0] == 2);
    assert(v[3] == 5);

    // iter over vector as const reference
    std::cout << "display v:" << std::endl;
    for(const auto& i: v) {
        std::cout << "item: " << i << std::endl;
        // uncomment to see compile error
        // i+=1;
    }

    // Note: see rvalue_references.cpp for other example

    return 0;
}