#include <iostream>
#include <cassert>

using namespace std;

class Base {
public:
    explicit Base(int value): value_(value) {};
    int value_;

    virtual int get_value() = 0;
    virtual void print_value() = 0;
};

class Base1: public Base {
public:
    explicit Base1(int value): Base(value) {};

    // Note: explicitly state get_value is overridden by the base class
    virtual int get_value() override {
        return value_+1;
    }

    // Error: get_value0 cannot be overridden (not defined in base class)
    /*
    virtual int get_value0() override {
        return value_+11;
    }
    */

    virtual void print_value() final {
        std::cout << "value: " << value_ << std::endl;
    }
};

class Base2: public Base1 {
public:
    explicit Base2(int value): Base1(value) {};

    // Note: no need for keyword virtual as function is marked with 'override'
    int get_value() override {
        return Base1::get_value()+1;
    }

    // Note
    // Cannot override print_value() as it is marked as 'final' in Base1
    /*
    virtual void print_value() final {
        std::cout << "[Base2] value: " << value_ << std::endl;
    }
     */

};

int main() {

    Base1 b1(42);
    b1.print_value();
    assert(b1.get_value() == 43);

    Base2 b2(42);
    assert(44 == b2.get_value());
    assert(43 == b2.Base1::get_value());

    return 0;
}
