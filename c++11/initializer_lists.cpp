#include <vector>
#include <map>
#include <algorithm>
#include <cassert>
#include <iostream>

int is_even(int x) {
    // std::cout << "is_even, x:" << x << " " << (x%2==0) << std::endl;
    return (x%2==0);
};

class SequenceClass
{
public:
    SequenceClass(std::initializer_list<int> list, bool all) {
        if (all) {
            vseq.insert(vseq.end(), list.begin(), list.end());
        }
        else {
            vseq.reserve((list.size() / 2)+1);
            std::copy_if(
                    list.begin(), list.end(),
                    std::back_inserter(vseq),
                    is_even
            );
        }
    };
    std::vector<int> vseq;
};

int main() {

    // using initializer list for vector and map
    std::vector<int> v {9, 8, 7, 6, 5, 4, 3, 2, 1};
    std::map<int, std::string> m {
            {0, "hello"},
            {1, "world"},
            {0, "!!"},
    };

    // now with SequenceClass
    SequenceClass sc1 {{1, 2, 3, 4}, true};
    assert(sc1.vseq.size() == 4);
    assert(sc1.vseq[0] == 1);
    assert(sc1.vseq[1] == 2);
    assert(sc1.vseq[2] == 3);
    assert(sc1.vseq[3] == 4);

    // only add even number from initializer list
    SequenceClass sc2 {{0, 1, 2, 3, 4}, false};
    assert(sc2.vseq.size() == 3);
    assert(sc2.vseq[0] == 0);
    assert(sc2.vseq[1] == 2);
    assert(sc2.vseq[2] == 4);
    return 0;
}

